
from urllib.error import HTTPError
from telebot.async_telebot import AsyncTeleBot
from telebot.types import Message
from pytube import YouTube
from pytube import Playlist
from pytube.cli import on_progress
import re
import asyncio
import traceback
import time

import mp3bot_strings


class mp3bot():
    
    __bot = None 
    __audio_path = "./audios"
    __log_file_path = "./mp3bot.log"
    __log_file = None
    log = None
    
    __max_download_retries = 3
    __delay_between_retry_s = 10

    def __init__(self, TOKEN) -> None:
        self.__bot = AsyncTeleBot(TOKEN)
        self.__log_file = open( self.__log_file_path , "w")



    def run(self) -> None:
        self.__bot.set_update_listener(self.__listener)
        self.define_commands()
        asyncio.run(self.__bot.infinity_polling(skip_pending = True))


    def log(self, log_message : str) -> None:
        self.__log_file.write( log_message + "\n")
        self.__log_file.flush()

    def define_commands(self) -> None:

        @self.__bot.message_handler(commands=['help', 'start'])
        async def send_welcome(message):
            await self.__bot.reply_to(message, mp3bot_strings.help)
    
    # Converts a YouTube URL to mp3 and returns said files path
    def __download(self, video_url : str) -> str:
    
        retry_counter = 0
        while retry_counter < self.__max_download_retries :
            # To not saturate youtube
            time.sleep(self.__delay_between_retry_s)
            try:
                self.log("Request to convert to mp3: " + video_url)
                yt = YouTube(video_url, on_progress_callback=on_progress)
                t = yt.streams.filter(only_audio= True)
                t[0].download(self.__audio_path)

                requested_audio_path = self.__audio_path + "/" + re.sub("""[\/.:~*?""<>|"]""", '', yt.title) + ".mp4"
                self.log("File to be sent: " + requested_audio_path)

                return requested_audio_path
            
            except Exception as e:
                retry_counter += 1
                self.log( f"RETRY N{retry_counter}- " + traceback.format_exc() )


    # Extract the first youtube url from a message
    def __extract_url_from_message(self, message : str) -> str:
        url = ""
        # Isolate YouTube URL from the message
        for portion in message.split():
            if "youtube.com/watch?" in portion or "youtube.com/playlist?" in portion:
                url = portion

        return url
    

            

    # Listener for chat messages
    async def __listener(self, messages: list[Message] ) -> None:
        try:

            for message in messages:

                # Check if in the message there is present a youtube url
                if message.reply_to_message is not None and ("youtube.com/watch?" in message.reply_to_message.text.lower() or "youtube.com/playlist?" in  message.reply_to_message.text.lower()):
                    
                    # Download an individual youtube video as mp3
                    if "!d" in message.text.lower():

                        video_url = self.__extract_url_from_message( str(message.reply_to_message.text) )
                        
                        download_path = self.__download(video_url)

                        # Send audio
                        audio = open( download_path , "rb")
                        await self.__bot.send_audio(chat_id= message.chat.id, audio=audio)

                        self.log("Audio with path " + download_path + " succesfully sent")

                    # Download all the videos in a playlist
                    elif "!ld" in message.text.lower():

                        # Check if it is indeed a playlist
                        if "?list=" in message.reply_to_message.text.lower() or "&list=" in message.reply_to_message.text.lower():
                        
                        
                            playlist_url = self.__extract_url_from_message( str(message.reply_to_message.text) )

                            self.log("Request to convert playlist to mp3: " + playlist_url)
                            playlist = Playlist(playlist_url)

                            self.log("Videos in playlist: " + str(playlist.video_urls))
                            for video_url in playlist.video_urls:
                                
                                audio_path = self.__download(video_url)
                                audio_file = open( audio_path , "rb")
                                await self.__bot.send_audio(chat_id= message.chat.id, audio= audio_file)

                                self.log("File " + audio_path + "succesfully sent\n")                             


                        else:
                            await self.__bot.reply_to(message, mp3bot_strings.playlist_incorrect)
                    
                    else:
                        await self.__bot.reply_to(message, mp3bot_strings.cant_convert)



        except Exception as e:
            self.log( traceback.format_exc() )



    def __exit__ (self):
        self.__log_file.close()